import os
working_dir = os.path.dirname(os.path.abspath(__file__))

from git import Repo
import os

import datetime as dt

def _get_d():
    return dt.date.today()

def _add_push():
    os.chdir(working_dir)
    repo = Repo.init(working_dir)
    print('Adding updates')
    repo.git.add('--all')
    print('Commiting')
    repo.index.commit(f"{_get_d()}")
    print('Pushing')
    repo.git.push()


def _pull():
    os.chdir(working_dir)
    repo = Repo.init(working_dir)
    print('Pulling')
    repo.git.pull()